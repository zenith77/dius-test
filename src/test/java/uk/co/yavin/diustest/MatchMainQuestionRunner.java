package uk.co.yavin.diustest;

import org.junit.Test;

public class MatchMainQuestionRunner {

	// simple test to run the main question as asked by the DIUS coding test
	// not really a test, but a convience for people simply wishing to run the main question
	// and see the answers on a console
	@Test
	public void mainQuestionConvienceRunner() {
		
		Match match = new Match("player 1", "player 2");
		match.pointWonBy("player 1");
		match.pointWonBy("player 2");
		// this will return "0-0, 15-15"
		System.out.println( match.score() );
	 
		match.pointWonBy("player 1");
		match.pointWonBy("player 1");
		// this will return "0-0, 40-15"
		System.out.println( match.score() );
	  
		match.pointWonBy("player 2");
		match.pointWonBy("player 2");
		// this will return "0-0, Deuce"
		System.out.println( match.score() );
	  
		match.pointWonBy("player 1");
		// this will return "0-0, Advantage player 1"
		System.out.println( match.score() );
	  
		match.pointWonBy("player 1");
		// this will return "1-0"
		System.out.println( match.score() );
	}
}