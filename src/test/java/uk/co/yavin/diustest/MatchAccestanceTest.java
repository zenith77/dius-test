package uk.co.yavin.diustest;

import org.junit.Assert;
import org.junit.Test;

// this class is MatchMainQuestionTest with asserts - to actually test the main question
public class MatchAccestanceTest {

	@Test
	public void mainTestWithAsserts() {

		Match match = new Match("player 1", "player 2");
		Assert.assertEquals( "0-0", match.score() );
		
		match.pointWonBy("player 1");
		match.pointWonBy("player 2");
		Assert.assertEquals( "0-0, 15-15", match.score() );
	 
		match.pointWonBy("player 1");
		match.pointWonBy("player 1");
		Assert.assertEquals( "0-0, 40-15", match.score() );
		 
		match.pointWonBy("player 2");
		match.pointWonBy("player 2");
		Assert.assertEquals( "0-0, Deuce", match.score() );

		match.pointWonBy("player 1");
		Assert.assertEquals( "0-0, Advantage player 1", match.score() );
	  
		match.pointWonBy("player 1");
		Assert.assertEquals( "1-0", match.score() );
	}
}