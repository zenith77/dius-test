package uk.co.yavin.diustest;

import org.junit.Assert;
import org.junit.Test;

public class PlayerStoreTest {

	@Test
	public void testBasics() {
		
		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		
		Assert.assertEquals(TestUtils.PLAYER_1, playerStore.getFirstPlayer().getName());
		Assert.assertEquals(TestUtils.PLAYER_2, playerStore.getSecondPlayer().getName());
	}
}
