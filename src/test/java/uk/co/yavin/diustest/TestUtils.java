package uk.co.yavin.diustest;

/**
 * class to put useful things for tests - like player names
 */
public class TestUtils {

	public static final String PLAYER_1 = "player 1";
	public static final String PLAYER_2 = "player 2";
}
