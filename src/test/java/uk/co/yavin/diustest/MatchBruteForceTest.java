package uk.co.yavin.diustest;

import java.util.Random;

import org.junit.Test;

import uk.co.yavin.diustest.gamestate.MatchEndedGameState;

// this test case simply runs the Match object for a number of times as per the SAMPLE size
public class MatchBruteForceTest {

	private static final Integer SAMPLE_SIZE = 1000;

	private static final String PLAYER_1 = "player 1";
	private static final String PLAYER_2 = "player 2";
	
	@Test
	public void testMatchBrueForceTest() {
		
		Random random = new Random();

		int playerOneWins = 0;
		int playerTwoWins = 0;

		for( int n = 0; n < SAMPLE_SIZE; n++ ) {
			
			// will be a number from 45-55
			Integer percentageChanceOfPlayerOnePointWin = random.nextInt(11) + 45;
			Match match = new Match(PLAYER_1, PLAYER_2);
			while( match.isInPlay() ) {
				
				Integer randomChance = random.nextInt(100) + 1;
				if( randomChance <= percentageChanceOfPlayerOnePointWin ) {
				
					match.pointWonBy(PLAYER_1);
				}
				else {
					
					match.pointWonBy(PLAYER_2);
				}
				//System.out.println( match.score() );
			}
			
			Player winner = ((MatchEndedGameState)match.getGameState()).getWinningPlayer();

			playerOneWins += winner.getName().equals(PLAYER_1) ? 1 : 0;
			playerTwoWins += winner.getName().equals(PLAYER_2) ? 1 : 0;
			
			boolean remarkable = (percentageChanceOfPlayerOnePointWin < 48 && winner.getName().equals(PLAYER_1)) ||
							     (percentageChanceOfPlayerOnePointWin > 52 && winner.getName().equals(PLAYER_2));
			
			System.out.println("Match " + (n+1) + " complete, ("+percentageChanceOfPlayerOnePointWin+"%), winner = " + winner.getName() + (remarkable ? "!" : "") );
		}
		
		System.out.println( "p1 = " + playerOneWins );
		System.out.println( "p2 = " + playerTwoWins );
	}
}
