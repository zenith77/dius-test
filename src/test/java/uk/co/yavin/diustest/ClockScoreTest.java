package uk.co.yavin.diustest;

import org.junit.Assert;
import org.junit.Test;


public class ClockScoreTest {

	@Test
	public void testBasic() {
		
		Assert.assertEquals( ClockScore.LOVE, ClockScore.fromNormalisedScore(0) ); 
		Assert.assertEquals( ClockScore.FIFTEEN, ClockScore.fromNormalisedScore(1) ); 
		Assert.assertEquals( ClockScore.THIRTY, ClockScore.fromNormalisedScore(2) ); 
		Assert.assertEquals( ClockScore.FORTY, ClockScore.fromNormalisedScore(3) ); 
	}
}