package uk.co.yavin.diustest.gamestate;

import org.junit.Test;

import org.junit.Assert;

import uk.co.yavin.diustest.ClockScore;
import uk.co.yavin.diustest.Player;
import uk.co.yavin.diustest.PlayerStore;
import uk.co.yavin.diustest.TestUtils;

public class StandardGameStateTest {
	
	// pointWonBy() tests
	
	@Test 
	public void testPointWonByToVictoryPlayer1() {
		
		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testPointWonByToVictoryBase( playerStore.getFirstPlayer(), playerStore.getSecondPlayer(), playerStore );
	}

	@Test
	public void testPointWonByToVictoryPlayer2() {

		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testPointWonByToVictoryBase( playerStore.getSecondPlayer(), playerStore.getFirstPlayer(), playerStore );
	}

	// called by other tests
	private void testPointWonByToVictoryBase(Player winner, Player loser, PlayerStore playerStore) {

		StandardGameState standardGameState = new StandardGameState( playerStore );

		// note: scores get switched with each different player!
		callPointsByWinAndAssert( winner.getName(), 15, 0,  true, false, standardGameState);
		callPointsByWinAndAssert( loser.getName(),  15, 15, true, false, standardGameState);
		callPointsByWinAndAssert( loser.getName(),  30, 15, true, false, standardGameState);
		callPointsByWinAndAssert( winner.getName(), 30, 30, true, false, standardGameState);
		callPointsByWinAndAssert( winner.getName(), 40, 30, true, false, standardGameState); 
		callPointsByWinAndAssert( winner.getName(), 0, 0,   false, false, standardGameState); // transitions to next game
	}

	@Test 
	public void testPointWonByToAdvantagePlayer1() {
		
		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testPointWonByToAdvantageBase( playerStore.getFirstPlayer(), playerStore.getSecondPlayer(), playerStore );
	}

	@Test
	public void testPointWonByToAdvantagePlayer2() {

		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testPointWonByToAdvantageBase( playerStore.getSecondPlayer(), playerStore.getFirstPlayer(), playerStore );
	}

	// called by other tests
	private void testPointWonByToAdvantageBase(Player winner, Player loser, PlayerStore playerStore) {

		StandardGameState standardGameState = new StandardGameState( playerStore );
		
		// note: scores get switched with each different player!
		callPointsByWinAndAssert( loser.getName(),  15, 0, true, false, standardGameState);
		callPointsByWinAndAssert( loser.getName(),  30, 0, true, false, standardGameState);
		callPointsByWinAndAssert( winner.getName(), 15, 30,  true, false, standardGameState);
		callPointsByWinAndAssert( winner.getName(), 30, 30, true, false, standardGameState);
		callPointsByWinAndAssert( winner.getName(), 40, 30, true, false, standardGameState); 
		callPointsByWinAndAssert( loser.getName(),  0, 0,   false, true, standardGameState); // transitions to advantage
	}
	

	// called by other tests - note: scores get switched with each different player!
	private void callPointsByWinAndAssert(String name, int winnerScore, int loserScore, Boolean sameInstance, Boolean advantageGameState, StandardGameState standardGameState) {

		GameState gameState = standardGameState.pointWonBy( name );
		Assert.assertEquals( sameInstance, gameState == standardGameState ); // same instance of gameState
		Assert.assertEquals( advantageGameState, gameState instanceof AdvantageGameState ); // same instance of gameState

		if( sameInstance ) {
			
			Player winner = standardGameState.getPlayerStore().getPlayerByName(name);
			Player loser = standardGameState.getPlayerStore().getOtherPlayer(winner);
			Assert.assertEquals( winnerScore, (int) ClockScore.fromNormalisedScore(standardGameState.getScores().get(winner)).getNumericClockScore() );
			Assert.assertEquals( loserScore,  (int) ClockScore.fromNormalisedScore(standardGameState.getScores().get(loser)).getNumericClockScore() );
		}
	}

	// checkVictoryCondition() tests

	@Test
	public void testCheckApplyVictoryConditionPlayer1() {

		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testCheckApplyVictoryConditionBase( playerStore.getFirstPlayer(), playerStore.getSecondPlayer(), playerStore );
	}

	@Test
	public void testCheckApplyVictoryConditionPlayer2() {

		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testCheckApplyVictoryConditionBase( playerStore.getSecondPlayer(), playerStore.getFirstPlayer(), playerStore );
	}

	// called by other tests
	private void testCheckApplyVictoryConditionBase( Player winner, Player loser, PlayerStore playerStore) {

		StandardGameState standardGameState = new StandardGameState( playerStore );
		
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 0, loser, 0, standardGameState, false, false);
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 0, loser, 1, standardGameState, false, false);
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 1, loser, 1, standardGameState, false, false);
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 2, loser, 1, standardGameState, false, false);
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 2, loser, 2, standardGameState, false, false);
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 3, loser, 2, standardGameState, false, false);
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 4, loser, 2, standardGameState, false, true); // to win!
	}

	// checkAdvantageState() tests
	
	@Test
	public void testCheckAdvantageStatePlayer1() {

		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testCheckAdvantageStateGameBase( playerStore.getFirstPlayer(), playerStore.getSecondPlayer(), playerStore );
	}

	@Test
	public void testCheckAdvantageStatePlayer2() {

		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testCheckAdvantageStateGameBase( playerStore.getSecondPlayer(), playerStore.getFirstPlayer(), playerStore );
	}
	
	// called by other tests
	private void testCheckAdvantageStateGameBase( Player winner, Player loser, PlayerStore playerStore) {

		StandardGameState standardGameState = new StandardGameState( playerStore );
		
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 0, loser, 0, standardGameState, false, false);
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 0, loser, 1, standardGameState, false, false);
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 1, loser, 1, standardGameState, false, false);
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 2, loser, 1, standardGameState, false, false);
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 2, loser, 2, standardGameState, false, false);
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 3, loser, 2, standardGameState, false, false);
		setPointsAndAssertForAdvantageAndVictoryConditions(winner, 3, loser, 3, standardGameState, true, false); // to advantage!
	}

	// called by other tests
	private void setPointsAndAssertForAdvantageAndVictoryConditions(Player winner, int winnerScore, Player loser, int loserScore, StandardGameState standardGameState, Boolean advantageState, Boolean winnerVictoryState) {

		standardGameState.getScores().put( winner, winnerScore );
		standardGameState.getScores().put( loser, loserScore );
		Assert.assertEquals( advantageState, standardGameState.checkAdvantageState() );
		Assert.assertEquals( winnerVictoryState, standardGameState.checkVictoryCondition( winner ) );
		Assert.assertFalse( standardGameState.checkVictoryCondition( loser ) );
	}
}
