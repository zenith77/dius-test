package uk.co.yavin.diustest.gamestate;

import org.junit.Assert;
import org.junit.Test;

import uk.co.yavin.diustest.Player;
import uk.co.yavin.diustest.PlayerStore;
import uk.co.yavin.diustest.TestUtils;

public class TieBreakGameStateTest {
	
	@Test 
	public void testPointsWonByNormalScenarioPlayer1() {
		
		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testPointWonByNormalScenarioBase( playerStore.getFirstPlayer(), playerStore.getSecondPlayer(), playerStore );
	}

	@Test 
	public void testPointsWonByNormalScenarioPlayer2() {
		
		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testPointWonByNormalScenarioBase( playerStore.getSecondPlayer(), playerStore.getFirstPlayer(), playerStore );
	}


	// called by other tests
	private void testPointWonByNormalScenarioBase(Player winner, Player loser, PlayerStore playerStore) {

		TieBreakGameState tieBreakGameState = new TieBreakGameState( playerStore );
		
		// note: scores get switched with each different player!
		callPointsByWinAndAssert( loser.getName(),  1, 0, true, false, tieBreakGameState);
		callPointsByWinAndAssert( loser.getName(),  2, 0, true, false, tieBreakGameState);
		callPointsByWinAndAssert( loser.getName(),  3, 0, true, false, tieBreakGameState);
		callPointsByWinAndAssert( loser.getName(),  4, 0, true, false, tieBreakGameState);
		callPointsByWinAndAssert( loser.getName(),  5, 0, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 1, 5, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 2, 5, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 3, 5, true, false, tieBreakGameState); 
		callPointsByWinAndAssert( winner.getName(), 4, 5, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 5, 5, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 6, 5, true, false, tieBreakGameState); 
		callPointsByWinAndAssert( winner.getName(), 0, 0, false, true, tieBreakGameState); // transitions to next standard game
	}
	

	@Test 
	public void testPointsWonByExtendedScenarioPlayer1() {
		
		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testPointWonByExtendedScenarioBase( playerStore.getFirstPlayer(), playerStore.getSecondPlayer(), playerStore );
	}

	@Test 
	public void testPointsWonByExtendedScenarioPlayer2() {
		
		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testPointWonByExtendedScenarioBase( playerStore.getSecondPlayer(), playerStore.getFirstPlayer(), playerStore );
	}


	// called by other tests
	private void testPointWonByExtendedScenarioBase(Player winner, Player loser, PlayerStore playerStore) {

		TieBreakGameState tieBreakGameState = new TieBreakGameState( playerStore );
		
		// note: scores get switched with each different player!
		callPointsByWinAndAssert( loser.getName(),  1, 0, true, false, tieBreakGameState);
		callPointsByWinAndAssert( loser.getName(),  2, 0, true, false, tieBreakGameState);
		callPointsByWinAndAssert( loser.getName(),  3, 0, true, false, tieBreakGameState);
		callPointsByWinAndAssert( loser.getName(),  4, 0, true, false, tieBreakGameState);
		callPointsByWinAndAssert( loser.getName(),  5, 0, true, false, tieBreakGameState);
		callPointsByWinAndAssert( loser.getName(),  6, 0, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 1, 6, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 2, 6, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 3, 6, true, false, tieBreakGameState); 
		callPointsByWinAndAssert( winner.getName(), 4, 6, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 5, 6, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 6, 6, true, false, tieBreakGameState); 
		callPointsByWinAndAssert( loser.getName(),  7, 6, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 7, 7, true, false, tieBreakGameState);
		callPointsByWinAndAssert( loser.getName(),  8, 7, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 8, 8, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 9, 8, true, false, tieBreakGameState);
		callPointsByWinAndAssert( winner.getName(), 0, 0, false, true, tieBreakGameState); // transitions to next standard game
	}
	
	// called by other test methods
	private void callPointsByWinAndAssert( String name, int winnerPoints, int loserPoints, Boolean sameInstance, Boolean standardGameState, TieBreakGameState tieBreakGameState ) {

		GameState gameState = tieBreakGameState.pointWonBy( name );
		Assert.assertEquals( sameInstance, gameState == tieBreakGameState ); // same instance of gameState
		Assert.assertEquals( standardGameState, gameState instanceof StandardGameState ); // same instance of gameState

		if( sameInstance ) {
			
			Player winner = tieBreakGameState.getPlayerStore().getPlayerByName(name);
			Player loser = tieBreakGameState.getPlayerStore().getOtherPlayer(winner);
			Assert.assertEquals( winnerPoints, (int) tieBreakGameState.getScores().get(winner) );
			Assert.assertEquals( loserPoints,  (int) tieBreakGameState.getScores().get(loser) );
		}
	}
	
	@Test
	public void testCheckVictoryConditionFalsePlayer1() {

		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testCheckVictoryConditionFalseBase( playerStore.getFirstPlayer(), playerStore );
	}

	@Test
	public void testCheckVictoryConditionFalsePlayer2() {

		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		testCheckVictoryConditionFalseBase( playerStore.getSecondPlayer(), playerStore );
	}

	// called by other test methods
	private void testCheckVictoryConditionFalseBase( Player player, PlayerStore playerStore ) {
		
		TieBreakGameState tieBreakGameState = new TieBreakGameState( playerStore );
		tieBreakGameState.getScores().put( player, 0 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(player) );

		tieBreakGameState.getScores().put( player, 1 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(player) );
		
		tieBreakGameState.getScores().put( player, 2 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(player) );
		
		tieBreakGameState.getScores().put( player, 3 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(player) );
		
		tieBreakGameState.getScores().put( player, 4 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(player) );
		
		tieBreakGameState.getScores().put( player, 5 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(player) );
		
		tieBreakGameState.getScores().put( player, 6 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(player) );
	}
	
	@Test
	public void testCheckVictoryConditionTrue() {
		
		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		TieBreakGameState tieBreakGameState = new TieBreakGameState( playerStore );
		
		tieBreakGameState.getScores().put( playerStore.getFirstPlayer(), 7 );
		tieBreakGameState.getScores().put( playerStore.getSecondPlayer(), 0 );
		Assert.assertTrue( tieBreakGameState.checkVictoryCondition(playerStore.getFirstPlayer()) );
		
		tieBreakGameState.getScores().put( playerStore.getFirstPlayer(), 0 );
		tieBreakGameState.getScores().put( playerStore.getSecondPlayer(), 7 );
		Assert.assertTrue( tieBreakGameState.checkVictoryCondition(playerStore.getSecondPlayer()) );
	}
	
	@Test
	public void testCheckVictoryConditionContinuesBeyondSeven() {
		
		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		TieBreakGameState tieBreakGameState = new TieBreakGameState( playerStore );
		tieBreakGameState.getScores().put( playerStore.getFirstPlayer(), 6 );
		tieBreakGameState.getScores().put( playerStore.getSecondPlayer(), 6 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getFirstPlayer()) );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getSecondPlayer()) );
		
		tieBreakGameState.getScores().put( playerStore.getFirstPlayer(), 7 );
		tieBreakGameState.getScores().put( playerStore.getSecondPlayer(), 6 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getFirstPlayer()) );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getSecondPlayer()) );
		
		tieBreakGameState.getScores().put( playerStore.getFirstPlayer(), 7 );
		tieBreakGameState.getScores().put( playerStore.getSecondPlayer(), 7 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getFirstPlayer()) );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getSecondPlayer()) );
		
		tieBreakGameState.getScores().put( playerStore.getFirstPlayer(), 8 );
		tieBreakGameState.getScores().put( playerStore.getSecondPlayer(), 7 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getFirstPlayer()) );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getSecondPlayer()) );
		
		tieBreakGameState.getScores().put( playerStore.getFirstPlayer(), 8 );
		tieBreakGameState.getScores().put( playerStore.getSecondPlayer(), 8 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getFirstPlayer()) );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getSecondPlayer()) );
		
		tieBreakGameState.getScores().put( playerStore.getFirstPlayer(), 8 );
		tieBreakGameState.getScores().put( playerStore.getSecondPlayer(), 9 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getFirstPlayer()) );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getSecondPlayer()) );
		
		tieBreakGameState.getScores().put( playerStore.getFirstPlayer(), 8 );
		tieBreakGameState.getScores().put( playerStore.getSecondPlayer(), 10 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getFirstPlayer()) );
		Assert.assertTrue( tieBreakGameState.checkVictoryCondition(playerStore.getSecondPlayer()) );
		
		tieBreakGameState.getScores().put( playerStore.getFirstPlayer(), 9 );
		tieBreakGameState.getScores().put( playerStore.getSecondPlayer(), 8 );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getFirstPlayer()) );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getSecondPlayer()) );
		
		tieBreakGameState.getScores().put( playerStore.getFirstPlayer(), 10 );
		tieBreakGameState.getScores().put( playerStore.getSecondPlayer(), 8 );
		Assert.assertTrue( tieBreakGameState.checkVictoryCondition(playerStore.getFirstPlayer()) );
		Assert.assertFalse( tieBreakGameState.checkVictoryCondition(playerStore.getSecondPlayer()) );
	}
}
