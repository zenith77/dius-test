package uk.co.yavin.diustest.gamestate;

import org.junit.Assert;
import org.junit.Test;

import uk.co.yavin.diustest.PlayerStore;
import uk.co.yavin.diustest.TestUtils;

public class AdvantageGameStateTest {

	@Test
	public void testGainAdvantage() {
		
		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		AdvantageGameState advantageGameState = new AdvantageGameState( playerStore );
		
		advantageGameState.setAdvantage( null );
		GameState gameState = advantageGameState.pointWonBy(TestUtils.PLAYER_1);
		Assert.assertEquals( playerStore.getFirstPlayer(), advantageGameState.getAdvantage() );
		Assert.assertTrue( gameState == advantageGameState );
		
		advantageGameState.setAdvantage( null );
		gameState = advantageGameState.pointWonBy(TestUtils.PLAYER_2);
		Assert.assertEquals( playerStore.getSecondPlayer(), advantageGameState.getAdvantage() );
		Assert.assertTrue( gameState == advantageGameState );
	}
	
	@Test
	public void testLoseAdvantage() {
		
		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		AdvantageGameState advantageGameState = new AdvantageGameState( playerStore );
		
		advantageGameState.setAdvantage( playerStore.getFirstPlayer() );
		GameState gameState = advantageGameState.pointWonBy(TestUtils.PLAYER_2);
		Assert.assertNull( advantageGameState.getAdvantage() );
		Assert.assertTrue( gameState == advantageGameState );
		
		advantageGameState.setAdvantage( playerStore.getSecondPlayer() );
		gameState = advantageGameState.pointWonBy(TestUtils.PLAYER_1);
		Assert.assertNull( advantageGameState.getAdvantage() );
		Assert.assertTrue( gameState == advantageGameState );
	}
	
	@Test
	public void testAdvantageVictory() {
		
		PlayerStore playerStore = new PlayerStore(TestUtils.PLAYER_1, TestUtils.PLAYER_2);
		AdvantageGameState advantageGameState = new AdvantageGameState( playerStore );
		
		advantageGameState.setAdvantage( playerStore.getFirstPlayer() );
		GameState gameState = advantageGameState.pointWonBy(TestUtils.PLAYER_1);
		Assert.assertTrue( gameState instanceof StandardGameState );
		
		advantageGameState.setAdvantage( playerStore.getSecondPlayer() );
		gameState = advantageGameState.pointWonBy(TestUtils.PLAYER_2);
		Assert.assertTrue( gameState instanceof StandardGameState );
	}
}
