package uk.co.yavin.diustest;

public class Player {

	private String name;
	private Integer games = 0;
	private Integer sets = 0;

	public Player(String playerName) {
		
		this.name = playerName;
	}

	public String getName() {
	
		return name;
	}
	
	public void setName(String name) {
	
		this.name = name;
	}
	
	public Integer getGames() {
	
		return games;
	}
	
	public void addGames() {
	
		this.games ++;
	}

	public void resetGames() {

		this.games = 0;
	}
	
	public Integer getSets() {
	
		return sets;
	}
	
	public void addSets() {
	
		this.sets ++;
	}
	
}