package uk.co.yavin.diustest;

import uk.co.yavin.diustest.gamestate.GameState;
import uk.co.yavin.diustest.gamestate.MatchEndedGameState;
import uk.co.yavin.diustest.gamestate.StandardGameState;

public class Match {

	private GameState gameState;
	private PlayerStore playerStore;

	public Match( String firstPlayerName, String secondPlayerName ) {
		
		setPlayerStore( new PlayerStore( firstPlayerName, secondPlayerName ) );
		
		setGameState( new StandardGameState( getPlayerStore() ) );
	}

	public void pointWonBy( String playerName ) {

		GameState nextGameState = getGameState().pointWonBy( playerName );
		setGameState( nextGameState );
	}

	// shows set score, followed by game score
	public String score() {
		
		StringBuilder result = new StringBuilder();
		
		result.append( getPlayerStore().getFirstPlayer().getGames() );
		result.append( "-" );
		result.append( getPlayerStore().getSecondPlayer().getGames() );
		result.append( getGameState().score() );

		return result.toString();
	}

	public boolean isInPlay() {

		return ! (this.getGameState() instanceof MatchEndedGameState);
	}

	protected PlayerStore getPlayerStore() {

		return playerStore;
	}
	
	protected void setPlayerStore(PlayerStore playerStore) {

		this.playerStore = playerStore;
	}
	
	protected GameState getGameState() {
	
		return gameState;
	}
	
	protected void setGameState(GameState gameState) {
	
		this.gameState = gameState;
	}
}