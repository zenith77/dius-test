package uk.co.yavin.diustest;

import java.util.HashMap;
import java.util.Map;

public class PlayerStore {

	private final Player firstPlayer;
	private final Player secondPlayer;
	private final Map<String, Player> playersByNameStore = new HashMap<>();

	public PlayerStore(String firstPlayerName, String secondPlayerName) {
		
		firstPlayer = new Player(firstPlayerName);
		secondPlayer = new Player(secondPlayerName);
		getPlayersByNameStore().put( firstPlayerName, firstPlayer );
		getPlayersByNameStore().put( secondPlayerName, secondPlayer );
	}
	
	public Player getPlayerByName( String playerName ) {
		
		if( getPlayersByNameStore().containsKey( playerName ) ) {
			
			return getPlayersByNameStore().get( playerName );
		}
		
		throw new IllegalStateException( playerName + " doesn't exist in playerStore" );
	}
	
	public Player getOtherPlayer( Player thisPlayer ) {
		
		for( Player candidate : this.getPlayersByNameStore().values() ) {
			
			if( ! candidate.equals(thisPlayer) ) {
				
				return candidate;
			}
		}
		
		throw new IllegalStateException( "another player doesn't exist in playerStore, only " + thisPlayer );
	}

	public Player getFirstPlayer() {
	
		return firstPlayer;
	}

	public Player getSecondPlayer() {
	
		return secondPlayer;
	}
	
	protected Map<String, Player> getPlayersByNameStore() {

		return playersByNameStore;
	}
}