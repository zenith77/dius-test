package uk.co.yavin.diustest.gamestate;

import uk.co.yavin.diustest.Player;
import uk.co.yavin.diustest.PlayerStore;
import uk.co.yavin.diustest.setstate.StandardSetState;

public interface GameState {

	GameState pointWonBy( String playerName );

	String score();
	
	PlayerStore getPlayerStore();
	
	default GameState applyVictoryCondition( Player winningPlayer ) {

		// normally this would be injected, retrieved from some settings/configuration, the Match object or passed in
		StandardSetState stateEngineService = new StandardSetState( getPlayerStore() );
		return stateEngineService.applyVictoryCondition( winningPlayer );
	}
}
