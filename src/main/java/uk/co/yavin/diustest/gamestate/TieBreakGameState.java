package uk.co.yavin.diustest.gamestate;

import java.util.HashMap;
import java.util.Map;

import uk.co.yavin.diustest.Player;
import uk.co.yavin.diustest.PlayerStore;

public class TieBreakGameState implements GameState {

	private final static int POINTS_TO_WIN = 7; // player needs to get this number of points to win
	private final static int POINTS_TO_WIN_BY_CLEAR = 2; // player needs to be 2 full points ahead to win a tie breaker
	
	private PlayerStore playerStore;
	private Map<Player,Integer> scores = new HashMap<>(); // 0-3, represents 0, 15, 30, 40 as per ClockScores enum

	public TieBreakGameState( PlayerStore playerStore ) {

		setPlayerStore( playerStore );
		this.getScores().put( this.getPlayerStore().getFirstPlayer(), 0 );
		this.getScores().put( this.getPlayerStore().getSecondPlayer(), 0 );
	}

	@Override
	public GameState pointWonBy( String playerName ) {
		
		Player player = this.getPlayerStore().getPlayerByName( playerName );
		Integer currentScore = this.getScores().get( player );
		Integer newScore = currentScore + 1;
		this.getScores().put( player, newScore );
		
		if( checkVictoryCondition( this.getPlayerStore().getFirstPlayer() ) ) {
			
			return applyVictoryCondition( this.getPlayerStore().getFirstPlayer() );
		}
		
		if( checkVictoryCondition( this.getPlayerStore().getSecondPlayer() ) ) {
			
			return applyVictoryCondition( this.getPlayerStore().getSecondPlayer() );
		}
		
		return this;
	}

	// when one player has four points
	protected boolean checkVictoryCondition( Player player ) {

		Integer score = this.getScores().get(player);
		Player otherPlayer = this.getPlayerStore().getOtherPlayer(player);
		Integer otherPlayerScore = this.getScores().get(otherPlayer);
		return score >= POINTS_TO_WIN && score >= otherPlayerScore + POINTS_TO_WIN_BY_CLEAR;
	}

	@Override
	public String score() {

		StringBuilder result = new StringBuilder();

		if( ! getFirstPlayerScore().equals(0) ||
		    ! getSecondPlayerScore().equals(0) ) {
			
			result.append( ", " );
			result.append( getFirstPlayerScore() );
			result.append( "-" );
			result.append( getSecondPlayerScore() );
		}
		
		return result.toString();
	}

	@Override
	public PlayerStore getPlayerStore() {

		return playerStore;
	}
	
	protected void setPlayerStore( PlayerStore playerStore ) {

		this.playerStore = playerStore;
	}
	
	protected Integer getFirstPlayerScore() {
	
		return this.getScores().get( this.getPlayerStore().getFirstPlayer() );
	}

	protected Integer getSecondPlayerScore() {

		return this.getScores().get( this.getPlayerStore().getSecondPlayer() );
	}
	
	protected Map<Player, Integer> getScores() {

		return this.scores;
	}
}