package uk.co.yavin.diustest.gamestate;

import uk.co.yavin.diustest.Player;
import uk.co.yavin.diustest.PlayerStore;

// effectively a dummy object, so end state isn't a null or something, that could cause errors.
public class MatchEndedGameState implements GameState {
	
	private Player winningPlayer;
	private PlayerStore playerStore;

	public MatchEndedGameState(Player winningPlayer, PlayerStore playerStore) {
		
		this.setWinningPlayer( winningPlayer );
		this.setPlayerStore( playerStore );
	}

	@Override
	public GameState pointWonBy(String playerName) {

		return this;
	}

	@Override
	public String score() {

		return "";
	}

	@Override
	public PlayerStore getPlayerStore() {

		return playerStore;
	}
	
	protected void setPlayerStore( PlayerStore playerStore ) {

		this.playerStore = playerStore;
	}
	
	public Player getWinningPlayer() {
	
		return winningPlayer;
	}
	
	protected void setWinningPlayer(Player winningPlayer) {
	
		this.winningPlayer = winningPlayer;
	}
}