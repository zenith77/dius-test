package uk.co.yavin.diustest.gamestate;

import java.util.HashMap;
import java.util.Map;

import uk.co.yavin.diustest.ClockScore;
import uk.co.yavin.diustest.Player;
import uk.co.yavin.diustest.PlayerStore;

public class StandardGameState implements GameState {
	
	private final static int POINTS_TO_DEUCE = 3;
	private final static int POINTS_TO_WIN = 4;
	
	private PlayerStore playerStore;
	private Map<Player,Integer> scores = new HashMap<>(); // 0-3, represents 0, 15, 30, 40 as per ClockScores enum

	public StandardGameState( PlayerStore playerStore ) {

		setPlayerStore( playerStore );
		this.getScores().put( this.getPlayerStore().getFirstPlayer(), 0 );
		this.getScores().put( this.getPlayerStore().getSecondPlayer(), 0 );
	}

	@Override
	public GameState pointWonBy( String playerName ) {
		
		Player player = this.getPlayerStore().getPlayerByName( playerName );
		Integer currentScore = this.getScores().get( player );
		Integer newScore = currentScore + 1;
		this.getScores().put( player, newScore );

		if( checkAdvantageState() ) {
			
			AdvantageGameState advantageScoring = new AdvantageGameState(getPlayerStore());
			return advantageScoring;
		}
		
		if( checkVictoryCondition( this.getPlayerStore().getFirstPlayer() ) ) {
			
			return applyVictoryCondition( this.getPlayerStore().getFirstPlayer() );
		}
		
		if( checkVictoryCondition( this.getPlayerStore().getSecondPlayer() ) ) {
			
			return applyVictoryCondition( this.getPlayerStore().getSecondPlayer() );
		}
		
		return this;
	}
	
	// when both players have three points
	protected boolean checkAdvantageState() {

		Integer firstPlayerScore = this.getFirstPlayerScore();
		Integer secondPlayerScore = this.getSecondPlayerScore();
		return firstPlayerScore.equals(POINTS_TO_DEUCE) && secondPlayerScore.equals(POINTS_TO_DEUCE);
	}

	// when one player has four points
	protected boolean checkVictoryCondition( Player player ) {

		Integer score = this.getScores().get(player);
		return score.equals(POINTS_TO_WIN);
	}

	@Override
	public String score() {

		StringBuilder result = new StringBuilder();

		if( ! getFirstPlayerScore().equals(0) ||
		    ! getSecondPlayerScore().equals(0) ) {
			
			result.append( ", " );
			result.append( getFirstPlayerClockScore().getNumericClockScore() );
			result.append( "-" );
			result.append( getSecondPlayerClockScore().getNumericClockScore() );
		}
		
		return result.toString();
	}

	@Override
	public PlayerStore getPlayerStore() {

		return playerStore;
	}
	
	protected void setPlayerStore( PlayerStore playerStore ) {

		this.playerStore = playerStore;
	}
	
	protected ClockScore getFirstPlayerClockScore() {
		
		return ClockScore.fromNormalisedScore( this.getFirstPlayerScore() );
	}
	
	protected ClockScore getSecondPlayerClockScore() {
		
		return ClockScore.fromNormalisedScore( this.getSecondPlayerScore() );
	}
	
	protected Integer getFirstPlayerScore() {
	
		return this.getScores().get( this.getPlayerStore().getFirstPlayer() );
	}

	protected Integer getSecondPlayerScore() {

		return this.getScores().get( this.getPlayerStore().getSecondPlayer() );
	}
	
	protected Map<Player, Integer> getScores() {

		return this.scores;
	}

}