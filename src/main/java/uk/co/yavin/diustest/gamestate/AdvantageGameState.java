package uk.co.yavin.diustest.gamestate;

import uk.co.yavin.diustest.Player;
import uk.co.yavin.diustest.PlayerStore;

public class AdvantageGameState implements GameState {
	
	private Player advantage; // null = deuce
	private PlayerStore playerStore;

	public AdvantageGameState( PlayerStore playerStore ) {

		setPlayerStore( playerStore );
	}

	@Override
	public GameState pointWonBy( String playerName ) {

		Player player = this.getPlayerStore().getPlayerByName( playerName );
		if( isDeuce() ) {
			
			setAdvantage( player );
			return this;
		}
		else if( ! getAdvantage().equals(player) ) {
			
			returnToDeuce();
			return this;
		}
		else {

			return applyVictoryCondition( getAdvantage() );
		}
	}

	protected void returnToDeuce() {
		
		setAdvantage( null );
	}

	protected boolean isDeuce() {

		return getAdvantage() == null;
	}

	@Override
	public String score() {

		StringBuilder result = new StringBuilder();
		
		if( isDeuce() ) {

			result.append( ", Deuce" );
		}
		else {
			
			result.append( ", Advantage " + advantage.getName() );
		}

		return result.toString();
	}
	
	@Override
	public PlayerStore getPlayerStore() {
	
		return playerStore;
	}
	
	protected void setPlayerStore(PlayerStore playerStore) {

		this.playerStore = playerStore;
	}
	
	public Player getAdvantage() {
	
		return advantage;
	}
	
	public void setAdvantage(Player advantage) {
	
		this.advantage = advantage;
	}
}