package uk.co.yavin.diustest;

// 0-3, in tennis is represented by 0, 15, 30, 40 during games.
// this class handles the conversion
public enum ClockScore {
	
	LOVE(0, 0),
	FIFTEEN(15, 1),
	THIRTY(30, 2),
	FORTY(40, 3),
	;
	
	private final Integer numericClockScore;
	private final Integer normalisedScore;
	
	private ClockScore( Integer numericClockScore, Integer normalisedScore ) {
		
		this.numericClockScore = numericClockScore;
		this.normalisedScore = normalisedScore;
	}
	
	public Integer getNumericClockScore() {

		return numericClockScore;
	}
	
	public Integer getNormalisedScore() {

		return normalisedScore;
	}

	public static ClockScore fromNormalisedScore(Integer playerScore) {

		for( ClockScore clockScore : values() ) {
			
			if( clockScore.getNormalisedScore().equals(playerScore) ) {
				
				return clockScore;
			}
		}
		
		throw new IllegalStateException("Clock score for score " + playerScore + " doesn't exist");
	}

}
