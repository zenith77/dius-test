package uk.co.yavin.diustest.setstate;

import uk.co.yavin.diustest.Player;
import uk.co.yavin.diustest.PlayerStore;
import uk.co.yavin.diustest.gamestate.GameState;

public interface SetState {

	PlayerStore getPlayerStore();
	
	GameState applyVictoryCondition(Player winningPlayer);

}
