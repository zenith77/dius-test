package uk.co.yavin.diustest.setstate;

import uk.co.yavin.diustest.gamestate.MatchEndedGameState;
import uk.co.yavin.diustest.gamestate.StandardGameState;
import uk.co.yavin.diustest.Player;
import uk.co.yavin.diustest.PlayerStore;
import uk.co.yavin.diustest.gamestate.GameState;
import uk.co.yavin.diustest.gamestate.TieBreakGameState;

// Reflects US Standard Rules - alternative rules can be created for 
// Wimbledon (with no tie break in final set) and 8-set Pro and 9-set pro
// Rules
public class StandardSetState implements SetState {

	private static final Integer GAMES_TO_WIN_SET = 6;
	private static final Integer GAMES_TO_WIN_BY_CLEAR = 2;
	private static final Integer MAX_GAMES = 7;
	private static final Integer SETS_TO_WIN = 2;
	private PlayerStore playerStore;

	public StandardSetState(PlayerStore playerStore) {
		
		this.setPlayerState( playerStore );
	}

	@Override
	public GameState applyVictoryCondition( Player winningPlayer ) {

		winningPlayer.addGames();
		
		Player losingPlayer = playerStore.getOtherPlayer(winningPlayer);
		
		if( winningPlayer.getGames() >= MAX_GAMES ) {

			// set won
			winningPlayer.addSets();
			winningPlayer.resetGames();
			losingPlayer.resetGames();
		}
		else if( winningPlayer.getGames() >= GAMES_TO_WIN_SET ) {
			
			if( winningPlayer.getGames() >= losingPlayer.getGames() + GAMES_TO_WIN_BY_CLEAR ) {
			
				// set won
				winningPlayer.addSets();
				winningPlayer.resetGames();
				losingPlayer.resetGames();
			}
			else if( winningPlayer.getGames().equals(GAMES_TO_WIN_SET) && losingPlayer.getGames().equals(GAMES_TO_WIN_SET) ) {
					
				// tie breaker
				TieBreakGameState tieBreakState = new TieBreakGameState(playerStore);
				return tieBreakState;
			}
		}
		
		if( winningPlayer.getSets().equals( SETS_TO_WIN ) ) {
			
			return new MatchEndedGameState( winningPlayer, playerStore );
		}
		
		StandardGameState standardScoring = new StandardGameState(playerStore);
		return standardScoring;
	}
	
	@Override
	public PlayerStore getPlayerStore() {
	
		return this.playerStore;
	}

	protected void setPlayerState(PlayerStore playerStore) {

		this.playerStore = playerStore;
	}
}